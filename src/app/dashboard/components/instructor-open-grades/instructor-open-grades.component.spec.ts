import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructorOpenGradesComponent } from './instructor-open-grades.component';
import { MostModule } from '@themost/angular';
import { ErrorService } from '@universis/common';
import { CoursesService } from 'src/app/courses/services/courses.service';
import { NgPipesModule } from 'ngx-pipes';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('InstructorOpenGradesComponent', () => {
  let component: InstructorOpenGradesComponent;
  let fixture: ComponentFixture<InstructorOpenGradesComponent>;

  const errorSvc = jasmine.createSpyObj('ErrorService,', ['getLastError', 'navigateToError', 'setLastError']);
  const coursesSvc = jasmine.createSpyObj('CoursesService,', ['getAllInstructors', 'getCourseExams', 'getCourseExam', 'getCourseClassExams',
                                          'getCourseExams2', 'getRecentCourses', 'getCourseCurrentExams', 'getAllClasses', 'getCourseClass',
                                          'getCourseClassStudents', 'searchCourseClassStudents', 'getCourseClassList', 'getStudentList',
                                          'getStudentBySearch', 'uploadGradesFile', 'getCurrentCourseExams', 'setUploadToComplete',
                                          'getUploadHistory', 'getUploadHistory']);

  coursesSvc.getCourseCurrentExams.and.returnValue(Promise.resolve(JSON.parse('{}')));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        MostModule.forRoot({
          base: '/',
          options: {
              useMediaTypeExtensions: false
          }
        }),
        NgPipesModule,
        TranslateModule.forRoot(),
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [ InstructorOpenGradesComponent ],
      providers:[
        {
          provide: ErrorService,
          useValue: errorSvc
        },
        {
          provide: CoursesService,
          useValue: coursesSvc
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructorOpenGradesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
