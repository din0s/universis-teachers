import {Component, Input, OnInit} from '@angular/core';
import { I18nPluralPipe } from '@angular/common';


@Component({
  selector: 'app-courses-grades-step-2',
  templateUrl: './courses-grades-step-2.component.html',
})
export class CoursesGradesStep2Component implements OnInit {

  @Input() resultsOfUpload;   // data with results
  @Input() courseExam;        // current courseExam object
  @Input() counters;          // counters with sums for every grade result state
  @Input() message = '';
  @Input() errorMessage = '';


  gradesUpdateMapping: any = {
    '=0': 'CoursesLocal.Modal.Step2.GradesUpdateDescription.none',
    '=1': 'CoursesLocal.Modal.Step2.GradesUpdateDescription.singular',
    'other': 'CoursesLocal.Modal.Step2.GradesUpdateDescription.plural'
  };

  gradesNotUpdateMapping: any = {
    '=0': 'CoursesLocal.Modal.Step2.GradesNotUpdateDescription.none',
    '=1': 'CoursesLocal.Modal.Step2.GradesNotUpdateDescription.singular',
    'other': 'CoursesLocal.Modal.Step2.GradesNotUpdateDescription.plural'
  };

  constructor() { }

  ngOnInit() {

  }

  // return the total number of grades that passed success
  getCountsSuccess() {
    let totalCount = 0;
    if  (this.counters) {
      for (const count of this.counters) {
        if (count.code === 'INS' || count.code === 'UPD') {
          totalCount += count.counter;
        }
      }
    }
    return totalCount;
  }

  // return the total number of grades that not passed success
  getCountsUnSuccess() {
    let totalCount = 0;
    if (this.counters) {
      for (const count of this.counters) {
        if (count.code === 'EAVAIL' || count.code === 'EFAIL' || count.code === 'ERANGE') {
          totalCount += count.counter;
        }
      }
    }
    return totalCount;
  }

}
