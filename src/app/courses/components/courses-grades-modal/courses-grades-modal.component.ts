import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import 'smartwizard';
import { FormBuilder} from '@angular/forms';
import {CoursesService} from '../../services/courses.service';
import {TranslateService} from '@ngx-translate/core';
import {LoadingService, ConfigurationService} from '@universis/common';
import {ErrorService} from '@universis/common';
import {Subject} from 'rxjs';
import {ProfileService} from '../../../profile/services/profile.service';
import WebSocketAsPromised from 'websocket-as-promised';
import { ApplicationSettings } from 'src/app/teachers-shared/teachers-shared.module';
import * as moment from 'moment';

declare var $: any;

@Component({
  selector: 'app-courses-grades-modal',
  templateUrl: './courses-grades-modal.component.html'
})

export class CoursesGradesModalComponent implements OnInit {

  public courseExamId: number;      // current courseExam ID
  public resultsOfUpload;           // results of uploading file
  public resultsOfSubmission;       // results of complete action
  public courseExam;                // current courseExam object
  public waitForResults = false;    // This flag help go to next step if validate is success
  public errorMessage = '';
  public websocketErrorMessage = '';
  objectKeys = Object.keys;
  //
  slots_info: any = {};
  certs_info: any = {};
  selected_slot = '';
  selected_cert = '';
  slotSelectedCertInfo: any = {};
  selected_source_encoding = 'base64';
  selected_destination_encoding = 'base64';
  output = '...';
  // private ws: WebSocketAsPromised;
  signingResult = '';
  instructorEmail = '';
  checkTokenInfo = false;
  wsURI = '';
  emailErrorMessage = '';
  expiredErrorMessage = '';
  certificatAttributes: any = {};
  //
  public message = '';
  public haveChangesGrades = false;
  public haveSignedDocument = false;
  public counters = [];                 // counters with sums for every grade result state
  public useDigitalSignature = false;   // institute configuration setting
  // Native app variables
  public socket_open = false;
  error = '';
  public ws: WebSocketAsPromised;
  public pluggedDevice = false;

  @ViewChild('wizard') wizard: ElementRef;
  public selectedFile: File;        // current selected file for upload

  public result: Subject<boolean> = new Subject<boolean>(); // result for modal closing
  public resultSuccessStatus = false;      // true : reload data

  constructor(private modalRef: BsModalRef,
              private fb: FormBuilder,
              private courseServices: CoursesService,
              private translate: TranslateService,
              private loadingService: LoadingService,
              private  errorService: ErrorService,
              private profile: ProfileService,
              private _configurationService: ConfigurationService) {
    const appSetting = this._configurationService.settings.app;
    this.checkTokenInfo = appSetting && (<ApplicationSettings>appSetting).checkTokenInfo;
    this.wsURI = appSetting && (<ApplicationSettings>appSetting).websocket;
  }

  ngOnInit() {
    const self = this;
    this.profile.getInstructor().then(res => {
      this.instructorEmail = res.email;
      this.useDigitalSignature = res.department.organization.instituteConfiguration.useDigitalSignature;
      if (this.useDigitalSignature) {
        this.ws = new WebSocketAsPromised(this.wsURI, {
          packMessage: data => JSON.stringify(data),
          unpackMessage: (data: string) => JSON.parse(data),
          attachRequestId: (data, requestId) => Object.assign({request_id: requestId}, data),
          extractRequestId: data => data && data.request_id
        });
        this.ws.onClose.addListener(() => {
          this.socket_open = false;
          this.displayWebSocketError();
        });
        this.openSocket();
      }
    }).catch( err => {
      return this.errorService.navigateToError(err);
    });
    //
    this.getCourseExam();

    $(this.wizard.nativeElement).smartWizard({
      anchorSettings: {
        anchorClickable: true,           // Enable/Disable anchor navigation
        enableAllAnchors: false,         // Activates all anchors clickable all times
        markDoneStep: true,              // add done css
        enableAnchorOnDoneStep: true,    // Enable/Disable the done steps navigation
      },
      useURLhash: false,
      showStepURLhash: false,
      lang: {
        next: this.translate.instant('CoursesLocal.Modal.Step1.Next'),
        previous: this.translate.instant('CoursesLocal.Modal.Buttons.Prev')
      },
      toolbarSettings: {
        toolbarExtraButtons: [
          $('<button></button>').text(this.translate.instant('CoursesLocal.Modal.Buttons.Cancel'))
            .addClass('btn btn-secondary btn-close')
            .on('click', function () {
              self.closeModal();
            })
        ]
      },
    });
    self.hideButtonPrevious(true);
    self.disabledButtonNext(true);

    // call next function and validate data
    $(this.wizard.nativeElement).on('leaveStep', function (e, anchorObject, stepNumber, stepDirection) {
      if (!self.selectedFile) {
        self.errorMessage = self.translate.instant('CoursesLocal.Modal.Step1.NoFoundFile');
        return false;
      }

      if (stepDirection === 'forward' && stepNumber === 0) {
        if (!self.waitForResults) {
          self.loadingService.showLoading();
          self.uploadGradesFile();
          return false;
        }
      } else if (stepDirection === 'forward' && stepNumber === 1) {
        if (self.haveChangesGrades && !self.waitForResults) {
          self.loadingService.showLoading();
          self.setUploadToComplete();
          return false;
        } else if (!self.haveChangesGrades) {
          return false;
        }
      } else if (stepDirection === 'backward' && stepNumber === 1) {
        if (!self.socket_open && self.useDigitalSignature) {
          self.displayWebSocketError();
          self.disabledButtonNext(true);
        }
        self.hideButtonPrevious(true);
      }
    });

    // runs at the loading step
    $(this.wizard.nativeElement).on('showStep', function (e, anchorObject, stepNumber, stepDirection) {
      if (stepNumber === 0 && stepDirection === 'backward') {
        self.counters = [];
        self.waitForResults = false;
        self.message = '';
        self.errorMessage = '';
        self.haveChangesGrades = false;
        self.hideButtonPrevious(true);
      }

      if (stepNumber === 0) {
        self.changeTextOnButtonNext(self.translate.instant('CoursesLocal.Modal.Step1.Next'));
      } else if (stepNumber === 1) {
        self.waitForResults = false;
        if (self.useDigitalSignature) {
          self.changeTextOnButtonNext(self.translate.instant('CoursesLocal.Modal.Step2.NextAndSign'));
        } else {
          self.changeTextOnButtonNext(self.translate.instant('CoursesLocal.Modal.Step2.NextAndSubmit'));
        }
        self.hideButtonNext(false);
        self.hideButtonPrevious(false);

        if (self.counters.some(c => c.code === 'INS') || self.counters.some(c => c.code === 'UPD')) {
          self.disabledButtonNext(false);
        } else {
          self.disabledButtonNext(true);
        }

      } else if (stepNumber === 2 && stepDirection === 'forward') {
        $(self.wizard.nativeElement).smartWizard('next');
        self.hideButtonNext(true);
        self.changeTextOnButtonClose(self.translate.instant('CoursesLocal.Modal.Buttons.Close'));
      }
    });
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  displayWebSocketError() {
    if (this._configurationService.settings.app && (<ApplicationSettings>this._configurationService.settings.app).websocket) {
      this.websocketErrorMessage = this.translate.instant('CoursesLocal.Modal.Step2.WebsocketNotOpen');
    } else {
      this.websocketErrorMessage = this.translate.instant('CoursesLocal.Modal.Step2.WebSocketNotSetInApp');
    }
  }

  openViaChromeExtension() {
    window.postMessage({ type: 'UNIVERSIS', text: 'Native app is running' }, '*');
  }

  async openSocket() {
    try {
      // window.postMessage({ type: 'UNIVERSIS', text: 'Native app is running' }, '*');
      await this.ws.open();
      this.socket_open = true;
      this.listSlots();
      this.websocketErrorMessage = '';
    } catch (error) {
      this.error = error;
      console.error(error);
      if (this.useDigitalSignature) {
        this.displayWebSocketError();
      }
    }
  }
  async listSlots() {
    try {
      this.selected_cert = '';
      const response = await this.sendRequest({
        command: 'list_slots'
      });
      this.slots_info = response.payload;
      //
      if (Object.keys(this.slots_info).length > 0) {
        this.pluggedDevice = true;
      } else {
        this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step2.TokenNotPresentMessage');
      }
      //
      if (Object.keys(this.slots_info).length === 1) {
        this.selected_slot = Object.keys(this.slots_info)[0];
        this.openSession();
      }
    } catch (error) {
      console.error(error);
    }
  }

  async openSession() {
    try {
      const response = await this.sendRequest({
        command: 'open_session',
        slot_serial: this.selected_slot
      });
      this.slots_info[this.selected_slot].open = true;
      this.loginSession();
      this.listCerts();
    } catch (error) {
      if (error.message ===  this.translate.instant('CoursesLocal.Modal.Step2.TokenNotPresentCode')) {
        this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step2.TokenNotPresentMessage');
      }
      console.error(error);
    }
  }

  async loginSession() {
    try {
      const response = await this.sendRequest({
        command: 'login_session',
        slot_serial: this.selected_slot
      });
      this.slots_info[this.selected_slot].logged = true;
    } catch (error) {
      console.error(error);
    }
  }

  async sendRequest(payload: any) {
    if (this.socket_open) {
      const response = await this.ws.sendRequest(payload);
      if (response.error) {
        throw new Error(response.error);
      }
      return response;
    }
    throw Error('Websocket not open.');
  }

  selectCert() {
    this.openSession();
    this.loginSession();
    this.listCerts();
    this.errorMessage = '';
    // Disable sign button
    this.disabledButtonNext(true);
    this.selected_cert = '';
  }

  async listCerts() {
    try {
      const response = await this.sendRequest({
        command: 'list_certs',
        slot_serial: this.selected_slot
      });
      const newPayload = new Map();
      this.certs_info = new Map();
      this.errorMessage = '';
      Object.entries(response.payload).forEach(entry => {
        let certEmail = '';
        const key = entry[0];
        const value = entry[1];
        const from = Date.parse(value['info']['not_valid_before']);
        const to = Date.parse(value['info']['not_valid_after']);
        const currentDate = new Date().getTime();
        const subject = value['info']['subject'];
        // Split subject key values to map
        const subjectSplit = subject.split(',').map(pair => pair.split('='));
        this.certificatAttributes = new Map(subjectSplit);
        const ou = this.certificatAttributes.get('OU'); // Personal certificate should contain OU property
        if (!ou) {
          return;
        } else {
          this.certificatAttributes.set('OU', ou.substring(0, ou.lastIndexOf('-') - 1)); // Display only Class A, Class B info
          this.certificatAttributes.set('fromDate', moment(value['info']['not_valid_before']).format('DD/MM/YYYY')); // Set from date
          this.certificatAttributes.set('toDate', moment(value['info']['not_valid_after']).format('DD/MM/YYYY')); // Set to date
        }
        if (key.length > 0) {
          certEmail = this.certificatAttributes.get('1.2.840.113549.1.9.1'); // Email property
          if (this.checkTokenInfo) { // If parameter is set in app.json to true
            // Check if certificate email belongs to the logged in user, check if certificate is expired
            if ((currentDate >= from  && currentDate <= to) && (this.instructorEmail === certEmail)) {
              this.certs_info.set(key, this.certificatAttributes);
              newPayload.set(key, value);
            }
            if (currentDate <= from  || currentDate >= to) {
              // Show error message for dates
              const fromDate = moment(value['info']['not_valid_before']).format('DD/MM/YYYY');
              const toDate = moment(value['info']['not_valid_after']).format('DD/MM/YYYY');
              const expiredDates =  ' (' + fromDate + '-' + toDate + ')';
              this.expiredErrorMessage = this.translate.instant('CoursesLocal.Modal.Step2.CertificateExpired') + expiredDates;
              this.errorMessage = this.expiredErrorMessage;
            }
            if (this.instructorEmail !== certEmail) {
              // Show error message for certificate email
              this.emailErrorMessage = this.translate.instant('CoursesLocal.Modal.Step2.CertificateEmailNotValid') + ' (' + certEmail + ')';
              this.errorMessage = this.emailErrorMessage;
            }
            if ((currentDate <= from  || currentDate >= to) && (this.instructorEmail !== certEmail)) {
              this.errorMessage = this.expiredErrorMessage + ' ' + this.emailErrorMessage;
            }
          } else {
            // Add object without checking for valid email, expiration
            this.certs_info.set(key, this.certificatAttributes);
            newPayload.set(key, value);
          }
        }
      });

      // this.certs_info2 = newPayload;
      this.slots_info[this.selected_slot].certificates = newPayload;
      if (this.certs_info.size === 1) {
        this.selected_cert = this.certs_info.keys().next().value;
      }
    } catch (error) {
      console.error(error);
    }
  }


  /**
   * ===============================================================
   * Getters
   * ===============================================================
   */

  get selectedSlotOpen() {
    return this.selected_slot !== '' && this.slots_info[this.selected_slot].open;
  }

  get selectedSlotLogged() {
    return this.selected_slot !== '' && this.slots_info[this.selected_slot].logged;
  }

  get selectedCert() {
    return this.selected_cert !== '';
  }

  get signFlag() {
    return this.socket_open
    && this.selectedSlotOpen
    && this.selectedSlotLogged
    && this.selectedCert;
  }

  get slotSelectedSlotInfo() {
    if (this.selected_slot) {
      return this.slots_info[this.selected_slot];
    }
    return false;
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  gradeSigning(additionalResult) {
    this.courseServices.signCourseExamDocument(this.courseExamId, this.resultsOfUpload.id, additionalResult).then(res => {
      const signedDocument = res.additionalResult.signedDocument;
      const signatureBlock = res.additionalResult.signatureBlock;
      const userCertificate = res.additionalResult.userCertificate;
      const checkHashKey = res.additionalResult.checkHashKey;
      if (signedDocument && signatureBlock && userCertificate && checkHashKey) {
        $(this.wizard.nativeElement).smartWizard('next'); // go to final step
        this.resultsOfSubmission.object.validationResult.success = true;
        this.haveSignedDocument = true;
      } else {
        this.resultsOfSubmission.object.validationResult.success = false;
        // Cancel the submitted CourseExamDocument
        this.cancelCourseExamDocument().then(_res => {
        }, (cancelErrr) => {
        console.error(cancelErrr);
      });
      }
    }, (err) => {
      console.error(err);
    });
  }

  cancelCourseExamDocument() {
    return this.courseServices.cancelCourseExamDocument(this.courseExamId, this.resultsOfUpload.id).then(cancelRes => {
      // If cancelled successfully
      if (cancelRes.actionStatus && cancelRes.actionStatus.alternateName === 'CancelledActionStatus') {
        this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step2.SigningFailed');
        this.message = this.translate.instant('CoursesLocal.Modal.Step2.CancelledSuccessfullyAtSigning');
      } else {
        this.message = '';
        const signingFailed = this.translate.instant('CoursesLocal.Modal.Step2.SigningFailed');
        const cancelFailedAtSigning = this.translate.instant('CoursesLocal.Modal.Step2.CancelFailedAtSigning');
        this.errorMessage =  signingFailed + ' ' + cancelFailedAtSigning;
      }
    }, (cancelErrr) => {
      console.log(cancelErrr);
    });
  }

  // second step
  uploadGradesFile() {
    if (this.selectedFile) {
      this.courseServices.uploadGradesFile(this.selectedFile, this.courseExamId).subscribe(res => {
        this.resultsOfUpload = res;

        if (this.resultsOfUpload.object.validationResult.success) {
          this.errorMessage = '';
          this.message = this.resultsOfUpload.object.validationResult.message;

          // group students by validation result code
          for (const grade of this.resultsOfUpload.object.grades) {
            if (grade.validationResult.code !== 'UNMOD') {
              this.haveChangesGrades = true;
              let index = -1;
              if (this.counters.length > 0) {
                index = this.counters.findIndex(c => c.code === grade.validationResult.code);
              }

              if (index >= 0) {
                this.counters[index].counter++;
              } else {
                this.counters.push(new Counter(
                  grade.validationResult.code,
                  1,
                  grade.validationResult.message
                ));
              }
            }
          }
          this.waitForResults = true;
          $(this.wizard.nativeElement).smartWizard('next'); // go to second step
          this.loadingService.hideLoading();
        } else {
          this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step2.NotValidFile');
          this.loadingService.hideLoading();
        }
      }, (err) => {
        // whne statusCode of validationResult exist get this
        const statusCode = err.error && err.error.object && err.error.object.validationResult ? err.error.object.validationResult.statusCode : 0;

        // get error message key
        const errorMessageKey = `CoursesLocal.Modal.Errors.E${statusCode || err.status || 500}.message`;
        // try to get localized message
        const errorMessage = this.translate.instant(errorMessageKey);
        // if custom error does not exist errorMessage should be equal to errorMessageKey
        if (errorMessage === errorMessageKey) {
          // get error message from common http errors
          this.errorMessage = this.translate.instant(`E${statusCode || err.status }.message`);
        } else {
          // set custom http error message
          this.errorMessage = errorMessage;
        }
        this.loadingService.hideLoading();
      });
    }
  }

  // final step
  setUploadToComplete() {
    if (this.selectedFile) {
      this.courseServices.setUploadToComplete(this.courseExamId, this.resultsOfUpload.id).then(res => {
        this.resultsOfSubmission = res;
        this.message = this.resultsOfSubmission.object.validationResult.message;

       if (this.resultsOfSubmission.object.validationResult.success) {
          if (this.resultsOfSubmission.object.validationResult.code === 'PSUCC') {
            this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step3.PartialSuccess');
          }
          this.hideButtonPrevious(true);
        } else {
          this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step3.NotSuccessSubmit');
          this.hideButtonPrevious(false);
        }
        this.resultSuccessStatus = true;
        this.waitForResults = true;
        if (this.useDigitalSignature) {
          // Add grade signing here
          this.createSigningResponse();
        }
        $(this.wizard.nativeElement).smartWizard('next'); // go to third step
         this.loadingService.hideLoading();
      }, (err) => {
        this.loadingService.hideLoading();

        if (this.useDigitalSignature) {
          this.cancelCourseExamDocument().then(res => {
            //
          }, (cancelErrr) => {
          console.log(cancelErrr);
        });
        } else {
          // get error message key
          const errorMessageKey = `CoursesLocal.Modal.Errors.E${err.status}.message`;
          this.waitForResults = true;
          $(this.wizard.nativeElement).smartWizard('next'); // go to third step
          // try to get localized message
          const errorMessage = this.translate.instant(errorMessageKey);
          // if custom error does not exist errorMessage should be equal to errorMessageKey
          if (errorMessage === errorMessageKey) {
            // get error message from common http errors
            this.errorMessage = this.translate.instant(`E${err.status}.message`);
          } else {
            // set custom http error message
            this.errorMessage = errorMessage;
          }
        }
      });
    }
  }

  async createSigningResponse() {
    if (this.selected_cert.length > 0) {
      try {
        const response = await this.sendRequest({
          command: 'sign_with_cert',
          slot_serial: this.selected_slot,
          cert_id: this.selected_cert,
          message: this.resultsOfSubmission.additionalResult.checkHashKey,
          source_encoding: this.selected_source_encoding,
          destination_encoding: this.selected_destination_encoding
        });
        this.output = response.payload.signature;
        // Get user certificate from selected cert key
        if (this.slotSelectedSlotInfo && this.slotSelectedSlotInfo['certificates'].size > 0 && this.selected_cert.length > 0) {
          this.slotSelectedSlotInfo['certificates'].forEach((value: Object, key: string) => {
            if (key === this.selected_cert) {
              this.slotSelectedCertInfo = value['info'];
              this.resultsOfSubmission.additionalResult.userCertificate = value['raw'];
              this.resultsOfSubmission.additionalResult.signatureBlock = this.output;
              this.errorMessage = '';
              this.gradeSigning(this.resultsOfSubmission.additionalResult);
            }
          });
        }
      } catch (error) {
        this.resultsOfSubmission.object.validationResult.success = false;
        this.cancelCourseExamDocument().then(res => {
        }, (cancelErrr) => {
          console.log(cancelErrr);
        });
        console.error(error);
      }
    } else {
      this.resultsOfSubmission.additionalResult.userCertificate = null;
      this.resultsOfSubmission.additionalResult.signatureBlock = null;
      this.errorMessage = this.translate.instant('CoursesLocal.Modal.Step2.CertificateNotValid');
      // Disable sign button
      const btnnext = document.getElementsByClassName('sw-btn-next')[0];
      btnnext.setAttribute('disabled', 'true');
    }
  }

  getCourseExam() {
    this.courseServices.getCurrentCourseExams(this.courseExamId).then(res => {
      this.courseExam = res;
      if (this.courseExam.status.alternateName === 'closed') {
        this.disabledButtonNext(true);
        this.errorMessage += this.translate.instant('CoursesLocal.Modal.Step1.ClosedExam');
      }
    }).catch(err => {
      return this.errorService.navigateToError(err);
    });
  }

  getInputFileName(newFile) {
    this.selectedFile = newFile;
    this.disabledButtonNext(false);
    this.errorMessage = '';
  }

  closeModal() {
    // If teacher should go through digital signing and has already submitted the xls (originalDocument has been created)
    if (this.useDigitalSignature && this.resultSuccessStatus && !this.haveSignedDocument) {
      this.cancelCourseExamDocument().then(res => {
        this.result.next(this.resultSuccessStatus);
        this.modalRef.hide();
      }, (cancelErrr) => {
        console.log(cancelErrr);
      });
    } else {
      this.result.next(this.resultSuccessStatus);
      this.modalRef.hide();
    }
  }
  disabledButtonNext(status) {
    const btnnext = document.getElementsByClassName('sw-btn-next')[0];
    if (status) {
      btnnext.setAttribute('disabled', 'true');
    } else {
      btnnext.removeAttribute('disabled');
    }
  }

  disabledButtonPrev(status) {
    const btnprev = document.getElementsByClassName('sw-btn-prev')[0];
    if (status) {
      btnprev.setAttribute('disabled', 'true');
    } else {
      btnprev.removeAttribute('disabled');
    }
  }

  hideButtonNext(status) {
    const btnnext = document.getElementsByClassName('sw-btn-next')[0];
    if (status) {
      btnnext.setAttribute('style', 'display:none');
    } else {
      btnnext.setAttribute('style', 'display:block');
    }
  }

  hideButtonPrevious(status) {
    const btnprev = document.getElementsByClassName('sw-btn-prev')[0];
    if (status) {
      btnprev.setAttribute('style', 'display:none');
    } else {
      btnprev.setAttribute('style', 'display:block');
    }
  }

  changeTextOnButtonNext(text) {
    const btnnext = document.getElementsByClassName('sw-btn-next')[0];
    btnnext.innerHTML = text;
  }

  changeTextOnButtonClose(text) {
    const btnnext = document.getElementsByClassName('btn btn-secondary btn-close')[0];
    btnnext.innerHTML = text;
  }

}

export class Counter {
  constructor(
    public code = '',
    public counter = 0,
    public message = ''
  ) {}
}
