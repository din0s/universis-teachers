import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';

@Component({
  selector: 'app-courses-grades-step-3',
  templateUrl: './courses-grades-step-3.component.html',
})
export class CoursesGradesStep3Component implements OnInit {

  @Input() resultsOfSubmission;            // data with results
  @Input() useDigitalSignature;
  @Input() errorMessage = '';
  @Input() message = '';
  @Input() courseExam;      // current courseExam object

  constructor() { }

  ngOnInit() {

  }


}
