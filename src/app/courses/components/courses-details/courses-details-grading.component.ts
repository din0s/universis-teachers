/**
 *
 * Courses details grading component
 *
 */

import { Component, OnInit, OnDestroy } from '@angular/core';
import { CoursesService } from '../../services/courses.service';
import {ErrorService} from '@universis/common';
import {ResponseError} from '@themost/client';
import {LoadingService} from '@universis/common';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-courses-details-grading',
  templateUrl: './courses-details-grading.component.html',
  styleUrls: ['./courses-details.component.scss']
})


export class CoursesDetailsGradingComponent implements OnInit, OnDestroy {

  public selectedClass: any;
  public exams: any;
  public courseExams: any;

  private routeParamsSubscription: Subscription;


  constructor(private  coursesService: CoursesService,
              private  errorService: ErrorService,
              private route: ActivatedRoute,
              private loadingService: LoadingService
  ) { }

  ngOnInit() {
    this.routeParamsSubscription = this.route.parent.params.subscribe(routeParams => {
      this.loadingService.showLoading();
      return this.coursesService.getCourseClass(
        routeParams.course,
        routeParams.year,
        routeParams.period
      ).then(courseClass => {
        if (typeof courseClass === 'undefined' && courseClass !== routeParams.course) {
          this.loadingService.hideLoading();
          throw new ResponseError('Course class cannot be found', 404);
        }

        return this.coursesService
          .getCourseClassExams(courseClass.id)
          .then(courseClassExams => {
            this.loadingService.hideLoading();
            this.courseExams = courseClassExams;
          });
      }).catch(err => {
        this.loadingService.hideLoading();
        return this.errorService.navigateToError(err);
      });
    });
  }

  ngOnDestroy(): void {
    this.routeParamsSubscription.unsubscribe();
  }
}
