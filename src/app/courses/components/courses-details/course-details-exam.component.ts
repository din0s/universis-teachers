/**
 *
 * Course details exam component
 *
 * Contains information about a specific exam.
 * Exam information is being fetched from the route.
 *
 */

import { Component, OnInit, OnDestroy } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {TranslateService} from '@ngx-translate/core';
import {CoursesService} from '../../services/courses.service';
import {CoursesSharedService} from '../../services/courses-shared.service';
import {ErrorService} from '@universis/common';
import {LoadingService} from '@universis/common';
import {GradeScaleService} from '@universis/common';
import {ProfileService} from '../../../profile/services/profile.service';
import {ResponseError} from '@themost/client';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-courses-details-exam',
  templateUrl: './course-details-exam.component.html',
  styleUrls: ['./courses-details.component.scss']
})
export class CoursesDetailsExamComponent implements OnInit, OnDestroy {

    public courseExam: any;
    public uploadHistory: any;
    public subDate: any;
    public loggedInstructor: any;
    public myHistory: any;

    // NOTE: this variable is private, but used in template
    // see: https://github.com/angular/angular/issues/11978
    // QUESTION: Should this field be private?
    private isLocked: any;

    constructor(private _context: AngularDataContext,
                private translate: TranslateService,
                private  coursesService: CoursesService,
                private coursesSharedService: CoursesSharedService,
                private  errorService: ErrorService,
                private router: Router,
                private route: ActivatedRoute,
                private loadingService: LoadingService,
                private gradeScaleService: GradeScaleService,
                private profileService: ProfileService) {
    }

  public studentGradesHistory: any;   // for Chart and Statistics
  public studentsRegisters = 0;
  public studentsGraded = 0;
  public studentsSuccessed = 0;
  public isLoading = true;
  // Charts values
  public chartGradesOptions: any;
  public chartGradesLabels: string[] ;
  public chartGradesData: any[] ;
  public chartGradesType = 'bar';
  public chartGradesLegend = true;
  public chartGradeColours: Array<any> = [
    { // blue
      backgroundColor: '#d9e1e6',
      borderColor: '#d9e1e6',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#d9e1e6',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#d9e1e6'
    }
  ];

  private routeParamsSubscription: Subscription;

  ngOnInit(): void {
    this.loadData();
  }

  ngOnDestroy(): void {
    this.routeParamsSubscription.unsubscribe();
  }

  loadData() {
    this.routeParamsSubscription = this.route.params.subscribe(routeParams => {
      this.loadingService.showLoading();

      // get parent route parameters inside subscription
      const course = this.route.snapshot.parent.parent.params.course;
      const year = this.route.snapshot.parent.parent.params.year;

      this.coursesService.getCourseExam(routeParams.courseExam)
        .then((courseExam) => {
          if (courseExam.course.id == course && year == courseExam.year.id) {
            this.coursesService.getUploadHistory(courseExam.id)
              .then((value) => {
                this.uploadHistory = value;
                this.profileService.getInstructor().then((logged) => {
                  this.loggedInstructor = logged;
                  this.myHistory = this.uploadHistory
                    .filter(x => x.additionalResult
                      &&
                      x.additionalResult.user.id === this.loggedInstructor.user
                    );
                  if (this.myHistory.length > 0) {
                    this.subDate = this.myHistory[0].additionalResult.dateCreated;
                  }
                  this.courseExam = courseExam;
                  this.isLocked = this.inferIsLocked();
                  this.loadStatistics();
                  this.loadingService.hideLoading();
                });
              }).catch(err => {
                this.loadingService.hideLoading();
                return this.errorService.navigateToError(err);
              });
          } else {
            throw new ResponseError('This Exam is not part of this Course', 404);
          }
        }).catch(err => {
          this.loadingService.hideLoading();
         return this.errorService.navigateToError(err);
        });
    });
  }

  inferIsLocked(): boolean {
    // course exam has been initialized
    return this.courseExam &&
      // user has been initialized
      this.loggedInstructor &&
      // has also upload actions
      this.uploadHistory &&
      // has at least one upload action
      this.courseExam.status.alternateName === 'inprogress' &&
      // upload action has additional result
      this.loggedInstructor.user !== this.courseExam.completedByUser;
  }

  importGrades(courseExam) {
      this.coursesSharedService.importCourseExamGradesDialog(courseExam).then(res => {
        if (res === true ) {
          this.loadData();  // reload page data
        }
     });
  }

  loadStatistics() {
    this.isLoading = true;
    this.gradeScaleService.getGradeScale(this.courseExam.course.gradeScale).then( gradeScale => {
      this.coursesService.getGradesStatistics(this.courseExam.id).then((result) => {
        this.studentGradesHistory = result.value;
        // get registered students
        this.studentsRegisters = result.value
        // map count
          .map(x => x.count)
          // calculate sum
          .reduce((a, b) => a + b, 0);

        // get students with grade
        this.studentsGraded = result.value
        // filter students by grade
          .filter(x => {
            return x.examGrade != null;
          })
          // map count
          .map(x => x.count)
          // calculate sum
          .reduce((a, b) => a + b, 0);

        // get students passed
        this.studentsSuccessed = result.value
        // filter passed grades
          .filter(x => {
            return x.isPassed;
          })
          // map count
          .map(x => x.count)
          // calculate sum
          .reduce( (a, b) => a + b, 0);

        // get grade scale values
        let gradeScaleValues = [];
        // numeric grade scale
        if (gradeScale.scaleType === 0) {
          // get grade scale base
          const gradeScaleMinValue = parseInt(gradeScale.format(gradeScale.scaleBase), 10);
          // get grade scale max
          const gradeScaleMaxValue = parseInt(gradeScale.format(1), 10);
          for (let grade = gradeScaleMinValue; grade < gradeScaleMaxValue; grade++) {
            gradeScaleValues.push({
              valueFrom: gradeScale.convert(grade),
              valueTo: gradeScale.convert(grade + 1),
              name: `${grade}-${grade + 1}`,
              total: 0
            });
          }
        } else {
          gradeScaleValues = gradeScale['values'].map ( value => {
            return {
              valueFrom: value.valueFrom,
              valueTo: value.valueTo,
              name: value.name,
              total: 0
            };
          });
        }
        // get total students per value
        gradeScaleValues.forEach( value => {
          value.total = result.value.filter( x => (x.examGrade === 1 && x.examGrade >= value.valueFrom && x.examGrade <= value.valueTo) ||
            (x.examGrade >= value.valueFrom && x.examGrade < value.valueTo))
            .map(x => x.count)
            .reduce( (a, b) => a + b, 0);
        });
        // get chart data
        const _chartGradesData: any = [{
          data: gradeScaleValues.map( x => x.total ),
          label: this.translate.instant('CoursesLocal.Chart.NumberOfStudents')
        }];
        const _chartGradesLabels: any = gradeScaleValues.map( x => x.name);
        // set chart options
        this.chartGradesOptions = {
          scaleShowVerticalLines: true,
          responsive: true,
          maintainAspectRatio: false,
          legend: {
            position: 'bottom',
          },
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true,
                precision: 0
              },
              scaleLabel: {
                display: true,
                labelString: this.translate.instant('CoursesLocal.Chart.NumberOfStudents')
              }
            }],
            xAxes: [{
              scaleLabel: {
                display: true,
                labelString: this.translate.instant('CoursesLocal.Chart.Grade')
              }
            }]
          }
        };
        this.chartGradesLabels = _chartGradesLabels;
        this.chartGradesData = _chartGradesData;

        this.isLoading = false;
        this.loadingService.hideLoading();
      });
    }).catch(err => {
      this.isLoading = false;
      this.loadingService.hideLoading();
      return this.errorService.navigateToError(err);
    });

  }

  exportGrades(courseExam, isCsv) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    if(isCsv){
      headers.set('Accept', 'text/csv');
    }
    const fileURL = this._context.getService().resolve(`instructors/me/exams/${courseExam.id}/students/export`);
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {

      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        const name = courseExam.name.replace(/[/\\?%*:|"<>]/g, '-');
        const extension = isCsv ? 'csv' : 'xlsx';
        a.download = `${name}-${courseExam.year.id}-${courseExam.examPeriod.name}.${extension}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove(); // remove the element
      });
  }

  historyGrades(attachments) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachments[0].url.replace(/\\/g, '/').replace('/api', '');
    const fileURL = this._context.getService().resolve(attachURL);
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {

      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachments[0].name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
      });
  }
}
