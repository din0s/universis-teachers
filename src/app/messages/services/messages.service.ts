import { Injectable } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  constructor(private _context: AngularDataContext,
              private _http: HttpClient) { }

  sendMessageToStudent(courseClassId, studentId, responseModel, actionId= null) {

    const formData: FormData = new FormData();
    if (responseModel.subject) {
      formData.append('subject', responseModel.subject);
    }
    if (actionId) {
      formData.append('action', actionId);
    }
    formData.append('body', responseModel.body);

    const serviceHeaders = this._context.getService().getHeaders();
    const postUrl = this._context.getService().resolve(`/instructors/me/classes/${courseClassId}/students/${studentId}/sendMessage`);

    return this._http.post(postUrl, formData, {
      headers: serviceHeaders
    });
  }


}
